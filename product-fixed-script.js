// GET IMAGE USING THUMB
var vtexUtils = new VTEX.VtexUtils();
// VtexHelpers
var vtexHelpers = vtexUtils.vtexHelpers;
// GlobalHelpers (from UtilifyJS)
var globalHelpers = vtexUtils.globalHelpers;
// LocationHelpers (from UtilifyJS)
var locationHelpers = vtexUtils.locationHelpers;
// Storage (from Store2 | https://github.com/nbubna/store)
var storage = vtexUtils.storage


if ($('body').width() > 810) {
    $(window).scroll(function () {
        var fixed = $(".produto-info-fixed");

        var fixed_position = $(".produto-info-fixed").offset().top;
        var fixed_height = $(".produto-info-fixed").height();

        var toCross_position = $('#footer-top').offset().top + $('#footer-top').height() - 800;

        if (fixed_position + fixed_height < toCross_position) {
            $('.produto-info-fixed').css('opacity', 1).css('pointer-events', 'unset');
        } else {
            $('.produto-info-fixed').css('opacity', 0).css('pointer-events', 'none');
        }
    });
}

jQuery(document).ready(function ($) {
    var $document = $(document),
        $element = $('.produto-info-fixed'),
        className = 'hasScrolled';


    $document.scroll(function () {
        if ($document.scrollTop() >= 800) {
            $element.addClass(className);
        } else {
            $element.removeClass(className);
        }
    });

    // var img_new = vtexHelpers.getResizedImage($('#image-main').attr('src'), 1000, 1000);

    // $('#detalhe-zoom-1 .img').attr('style', "background:url(" + img_new + ") center center no-repeat;background-size:60%;height: 500px");
    // $('#detalhe-zoom-2 .img').attr('style', "background:url(" + img_new + ") center center no-repeat;background-size:120%;height: 500px");
    // $('#image-main').attr('src', $('.image-zoom').attr('href'));
    // $('#image-main').attr('src', '' + img_new + '');


    if ($('#divTitulo').length > 0) {
        $('#divCompreJunto').prepend('<div class="row px-0 px-md-5"> <div class="col text-center"> <div class="titulo-duas-cores"> <h3 class="fs-16 yellow-color">COMPRE <span>JUNTO</span></h3> </div> </div> </div>');
    }

    jQuery('#numero_de_parcelas').html(jQuery('.skuBestInstallmentNumber').text());
    jQuery('#valor_das_parcelas').html(jQuery('.skuBestInstallmentValue').text());
    jQuery('.skuBestInstallmentNumber').text();

    if ($('#divCompreJunto').html().length == 0) {
        $('#compre-junto').remove();
    }


    jQuery('.thumbs').detach().appendTo('.produto-dealhes-interno');
    jQuery('.thumbs').addClass('mt-5');
    jQuery('.thumbs > li a img').each(function () {
        var text = $(this).attr('src').replace('55-55', '250-250');
        $(this).attr('src', text);
    });

    // jQuery('.thumbs li a').click(function () {
    //     $('#image-main').attr('src', $(this).attr('zoom'));
    //     $('#image a').attr('href', $(this).attr('zoom'));
    // });
});