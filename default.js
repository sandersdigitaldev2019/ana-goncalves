/*
 *
 * Desenvolvido por Integrando.se
 * hello@integrando.se
 * 
 * Bootstrap v.3
 *
 */

;


$(document).ready(function () {

    $mobile = 810;

    if ($.fn.ADMAKEadvancedFilter) {
        $(document).ADMAKEadvancedFilter({
            tipoFiltros: {},
        });
    }
    if ($.fn.ADMAKEmenu) {
        $(document).ADMAKEmenu();
    }
    $('.col-mini-cart').ADMAKEminiCart({
        miniCartQtd: '.mini-cart-qty-admake',
    });

    var $btnComprar = $('.btn-add-buy-button-asynchronous');
    if ($btnComprar.length) {
        $btnComprar.html('Comprar <i class="fa fa-lock"></i>');
    }

    var $btnComprarProduto = $('.buy-button.buy-button-ref');
    if ($btnComprarProduto.length) {
        if ($('#comprar-flutuante').length) {
            var $comprarFlutuante = $('#comprar-flutuante');
            var btnComprarTop = $('.product-info .buy-button-box').offset().top;
            $(window).scroll(function () {
                if ($(window).width() > 768) {
                    if ($(this).scrollTop() >= btnComprarTop && !$comprarFlutuante.is(':visible')) {
                        $comprarFlutuante.fadeIn(function () {
                            var urlImage = ($('#include #image-main').attr('src') != '') ? $('#include #image-main').attr('src') : '/arquivos/sem-foto.gif';
                            $('#foto-comprar-flutuante').attr('src', urlImage);
                            $('body').css('padding-bottom', $comprarFlutuante.height() + 30);
                        });
                    } else if ($(this).scrollTop() < btnComprarTop && $comprarFlutuante.is(':visible')) {
                        $comprarFlutuante.fadeOut(function () {
                            $('body').css('padding-bottom', 0);
                        });
                    }
                }
            });
        }


        $btnComprarProduto.html('Comprar <i class="fa fa-lock"></i>');

        $btnComprarProduto.click(function () {
            var $this = $(this);
            var url = $this.attr('href');
            if (url.indexOf('qty=1') > 0) {
                $this.attr('href', url.replace('qty=1', 'qty=' + parseInt($('.quantitySelector input').val())));
            }
        });

        var $recebeQtyForm = $btnComprarProduto.parents('.buy-button-box');
        if ($recebeQtyForm.length) {
            $recebeQtyForm.prepend(
                '<div class="pull-left box-qtd">' +
                '	<input type="text" class="qtd pull-left" value="1" />' +
                '	<div class="bts pull-left">' +
                '		<button class="btn btn-mais">+</button>' +
                '		<button class="btn btn-menos">-</button>' +
                ' 	</div>' +
                '</div>'
            );
            $(document).on('keypress', '.buy-button-box .box-qtd .qtd', function (e) {
                var tecla = (window.event) ? event.keyCode : e.which;
                if ((tecla > 47 && tecla < 58)) {
                    return true;
                } else {
                    if (tecla == 8 || tecla == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            $(document).on('keyup', '.buy-button-box .box-qtd .qtd', function (e) {
                $('.buy-button-box .box-qtd .qtd').val($(this).val());
            });
            $(document).on('blur', '.buy-button-box .box-qtd .qtd', function (e) {
                var $this = $(this);
                if ($this.val() === '' || parseInt($this.val()) < 1) {
                    $('.buy-button-box .box-qtd .qtd').val(1);
                } else {
                    $('.buy-button-box .box-qtd .qtd').val($this.val());
                }
            });
            $(document).on('click', '.buy-button-box .box-qtd .btn', function () {
                var $this = $(this);
                var $qtd = $('.buy-button-box .box-qtd .qtd');
                var valor = parseInt($qtd.val());
                if ($this.hasClass('btn-mais')) {
                    $qtd.val(valor + 1);
                } else if ($this.hasClass('btn-menos')) {
                    if (valor > 1) {
                        $qtd.val(valor - 1);
                    }
                }
            });
        }
    }

    //SLICK
    var $fullbanner = $(".home-banner ul");
    if ($fullbanner.length) {
        $($fullbanner).slick({
            infinite: false,
            autoplay: true,
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed: 3000
        });
    }

    if ($('body').width() < $mobile) {
        //MOBILE
        $('.prateleira > ul').slick({
            infinite: false,
            autoplay: true,
            arrows: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplaySpeed: 3000
        });
    } else {
        //DESKTOP
        $('.prateleira > ul').each(function (index, item) {
            let li = $(item).find('li');

            if (li.length > 3) {
                $(item).slick({
                    infinite: false,
                    autoplay: true,
                    arrows: true,
                    dots: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    autoplaySpeed: 3000
                });
            }
        });
    }
});

var wishList = function () {
    if (localStorage.getItem('wishList')) {
        function products() {
            $('body.wishlist main ul li').remove();

            let list = localStorage.getItem('wishList').split(',');

            $(list).each(function (index, item) {
                if (item != '') {
                    $.ajax({
                        url: '/api/catalog_system/pub/products/search?fq=productId:' + item,
                        type: 'GET'
                    }).done(function (response) {
                        let product = response[0];

                        console.log(product);

                        var por = '';
                        var installments = '';
                        var installmentsValue = '';

                        $(product.items).each(function (index, item) {
                            if (item.sellers[0].commertialOffer.AvailableQuantity != 0) {
                                por = item.sellers[0].commertialOffer.Price;
                                installments = product.sellers[0].commertialOffer.Installments[0].NumberOfInstallments;
                                installmentsValue = product.sellers[0].commertialOffer.Installments[0].Value;
                            }
                        });

                        let content = '';
                        content += '<li>';
                        content += '<div class="content">';
                        content += '<span id="favoritar" data-id="'+product.productId+'" class="active"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -28 512.001 512"><path d="m256 455.515625c-7.289062 0-14.316406-2.640625-19.792969-7.4375-20.683593-18.085937-40.625-35.082031-58.21875-50.074219l-.089843-.078125c-51.582032-43.957031-96.125-81.917969-127.117188-119.3125-34.644531-41.804687-50.78125-81.441406-50.78125-124.742187 0-42.070313 14.425781-80.882813 40.617188-109.292969 26.503906-28.746094 62.871093-44.578125 102.414062-44.578125 29.554688 0 56.621094 9.34375 80.445312 27.769531 12.023438 9.300781 22.921876 20.683594 32.523438 33.960938 9.605469-13.277344 20.5-24.660157 32.527344-33.960938 23.824218-18.425781 50.890625-27.769531 80.445312-27.769531 39.539063 0 75.910156 15.832031 102.414063 44.578125 26.191406 28.410156 40.613281 67.222656 40.613281 109.292969 0 43.300781-16.132812 82.9375-50.777344 124.738281-30.992187 37.398437-75.53125 75.355469-127.105468 119.308594-17.625 15.015625-37.597657 32.039062-58.328126 50.167969-5.472656 4.789062-12.503906 7.429687-19.789062 7.429687zm-112.96875-425.523437c-31.066406 0-59.605469 12.398437-80.367188 34.914062-21.070312 22.855469-32.675781 54.449219-32.675781 88.964844 0 36.417968 13.535157 68.988281 43.882813 105.605468 29.332031 35.394532 72.960937 72.574219 123.476562 115.625l.09375.078126c17.660156 15.050781 37.679688 32.113281 58.515625 50.332031 20.960938-18.253907 41.011719-35.34375 58.707031-50.417969 50.511719-43.050781 94.136719-80.222656 123.46875-115.617188 30.34375-36.617187 43.878907-69.1875 43.878907-105.605468 0-34.515625-11.605469-66.109375-32.675781-88.964844-20.757813-22.515625-49.300782-34.914062-80.363282-34.914062-22.757812 0-43.652344 7.234374-62.101562 21.5-16.441406 12.71875-27.894532 28.796874-34.609375 40.046874-3.453125 5.785157-9.53125 9.238282-16.261719 9.238282s-12.808594-3.453125-16.261719-9.238282c-6.710937-11.25-18.164062-27.328124-34.609375-40.046874-18.449218-14.265626-39.34375-21.5-62.097656-21.5zm0 0"></path></svg></span>';
                        content += '<a href="' + product.link + '"><img src="' + product.items[0].images[0].imageUrl + '" /></a>';
                        content += '<div class="details">';    
                            content += '<p class="name mt-3 mb-3 text-left name lh-1 text-uppercase fs-16 medium-grey-color w-100">' + product.productName + '</p>';
                                content += '<div class="price">';
                                content += '<p class="price sale mb-3 text-left fs-13 lh-1 text-uppercase yellow-color">POR ' + por.toString().replace('.',',') + '</p>';
                                content += '<span class="installment mb-3 text-left fs-13 lh-1 medium-grey-color w-100 installment d-none">';
                                content += 'Ou <strong class="yellow-color fs-15 font-weight-light">'+ installments +'</strong> de ';
                                content += '<strong class="yellow-color fs-15 font-weight-light">'+ installmentsValue +'</strong> sem juros';
                                content += '</span>';
                                content += '</div>';
                                content += '<span class="buy-btn"><a href="'+product.link+'" class="black-bg text-white text-uppercase fs-11 px-4 py-1">comprar</a></span>';
                            content += '</div>';
                        content += '</div>';
                        content += '</li>';

                        $('body.wishlist main ul').append(content);
                    });
                }
            });
        };
        products();

        function list() {
            let list = localStorage.getItem('wishList').split(',');

            $(list).each(function (index, item) {
                $('#favoritar[data-id="' + item + '"]').addClass('active');
            });
        };
        list();
    }

    function favoritar() {
        $(document).on('click', '#favoritar', function () {
            let item = $(this).data('id') + ',';

            if ($(this).hasClass('active')) {
                //REMOVENDO
                let newWishList = localStorage.getItem('wishList').replace(item, '');

                localStorage.setItem('wishList', newWishList);

                $(this).removeClass('active');

                //IN WISHLIST
                if ($('body.wishlist').length) {
                    products();
                };
            } else {
                if (localStorage.getItem('wishList')) {
                    //INCLUINDO
                    localStorage.setItem('wishList', localStorage.getItem('wishList') + item);
                } else {
                    //INCLUINDO
                    localStorage.setItem('wishList', item);
                }

                $(this).addClass('active');
            }
        });
    };
    favoritar();
};

wishList();